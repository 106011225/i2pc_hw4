#ifndef __SHA256_HEADER__
#define __SHA256_HEADER__

#include <stddef.h>
#include <cuda_runtime.h>

#ifdef __cplusplus
extern "C"{
#endif  //__cplusplus

//--------------- DATA TYPES --------------
typedef unsigned int WORD;
typedef unsigned char BYTE;

typedef union _sha256_ctx{
	WORD h[8];
	BYTE b[32];
}SHA256;


#define NUMBLOCKS 80
#define THREADS_PER_BLOCK 512

//----------- FUNCTION DECLARATION --------
void sha256_transform(SHA256 *ctx, const BYTE *msg);
void sha256(SHA256 *ctx, const BYTE *msg, size_t len);

__device__ void sha256_transform_dev(SHA256 *ctx, BYTE *msg);
__device__ void double_sha256_dev(SHA256 *ctx, BYTE *msg, BYTE* smem);


#ifdef __cplusplus
}
#endif  //__cplusplus

#endif  //__SHA256_HEADER__
