#NVFLAGS := -std=c++11 -O3 
NVFLAGS := -std=c++11 -O3 -Xptxas=-v -arch=sm_61 --relocatable-device-code=true
TARGET := hw4

.PHONY: all
all: $(TARGET)

#$(TARGET): sha256.o hw4.cu
#	nvcc $(NVFLAGS) -o hw4 hw4.cu sha256.o
$(TARGET): hw4.cu sha256.cu
	nvcc $(NVFLAGS) -o hw4 hw4.cu sha256.cu

sha256.o: sha256.cu
	nvcc $(NVFLAGS) -c sha256.cu

clean:
	rm -rf hw4 *.o




