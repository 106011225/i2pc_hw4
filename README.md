# Parallel computing: Bitcoin miner with **CUDA**


## Goal

- Given a sequential version of sample code of the bitcoin miner, parallelize it with CUDA
- More specifically, find out a proper hash value for a given block header
    - The hash value has to be less than the Target Difficulty

## Input 

- First line of the input is an integer N which represents number of blocks to solve
- Follow by N blocks of below data
    |Data|Description|
    |----|-----------|
    |20000000|Version|
    |0000000000000000003348540cbfc68b70825e7abcd5a83a48a5f87fa7f1aace|Previous block hash|
    |5ac22f8b|Timestamp|
    |17502ab7|Nbits (difficulty)|
    |2094|Number of transactions|
    |c6574adb277efbfb972658ab78b1277707a967076cfc90d6af800cd8a915396d|Transactions (2094 line of hash values like this)|


## Output
- First line of your output is an integer N represent number of blocks
- Follow by N line of nounce value


## Usage

1. Compile and execute the source code
    ```
    $ make
    $ ./hw4 testcases/caseXX.in caseXX.out
    ```
    where `testcases/caseXX.in` is the input testcase and `caseXX.out` is the file name of the output file


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
