//***********************************************************************************
// 2018.04.01 created by Zexlus1126
//
//    Example 002
// This is a simple demonstration on calculating merkle root from merkle branch 
// and solving a block (#286819) which the information is downloaded from Block Explorer 
//***********************************************************************************

#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cstring>
#include <cassert>

#include "sha256.h"

////////////////////////   Block   /////////////////////

typedef struct _HashBlock {
    unsigned int version;
    unsigned char prevhash[32];
    unsigned char merkle_root[32];
    unsigned int ntime;
    unsigned int nbits;
    unsigned int nonce;
} HashBlock;

#define CHECK_PERIOD 100
__constant__ HashBlock blockHeader_cnst;
volatile __constant__ unsigned char target_hex_cnst[32]; // w/o volatile: used 32->78 reg

////////////////////////   Utils   ///////////////////////

//convert one hex-codec char to binary
unsigned char decode(unsigned char c) {
    switch(c) {
        case 'a':
            return 0x0a;
        case 'b':
            return 0x0b;
        case 'c':
            return 0x0c;
        case 'd':
            return 0x0d;
        case 'e':
            return 0x0e;
        case 'f':
            return 0x0f;
        case '0' ... '9':
            return c-'0';
        default: // used to fix the compiler warnings
            exit(1); return 0;
    }
}



// The macro can be wrapped around any function returning a value of type `cudaError_t`.
inline cudaError_t checkCuda(cudaError_t result) {
  if (result != cudaSuccess) {
    printf("CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}


// convert hex string to binary
//
// in: input string
// string_len: the length of the input string
//      '\0' is not included in string_len!!!
// out: output bytes array
void convert_string_to_little_endian_bytes(unsigned char* out, char *in, size_t string_len) {
    assert(string_len % 2 == 0);

    size_t s;
    size_t b;
    for(s=0, b=string_len/2-1; s < string_len; s+=2, --b) // little endian
        out[b] = (unsigned char)(decode(in[s])<<4) + decode(in[s+1]); // 2 char represent 1 byte (8 bits)
}

// print out binary array (from highest value) in the hex format
void print_hex(unsigned char* hex, size_t len) {
    for(int i=0;i<len;++i)
        printf("%02x", hex[i]);
}

// print out binar array (from lowest value) in the hex format
void print_hex_inverse(unsigned char* hex, size_t len) {
    for(int i=len-1;i>=0;--i)
        printf("%02x", hex[i]);
}

int little_endian_bit_comparison(const unsigned char *a, const unsigned char *b, size_t byte_len) {
    // compared from lowest bit
    for(int i=byte_len-1; i>=0; --i) {
        if(a[i] < b[i])
            return -1;
        else if(a[i] > b[i])
            return 1;
    }
    return 0;
}


void getline(char *str, size_t len, FILE *fp) {
    int i=0;
    while( i<len && (str[i] = fgetc(fp)) != EOF && str[i++] != '\n');
    str[len-1] = '\0';
}

////////////////////////   Hash   ///////////////////////

void double_sha256(SHA256 *sha256_ctx, unsigned char *bytes, size_t len) {
    SHA256 tmp;
    sha256(&tmp, (BYTE*)bytes, len);
    sha256(sha256_ctx, (BYTE*)&tmp, sizeof(tmp));
}


////////////////////   Merkle Root   /////////////////////


// calculate merkle root from several merkle branches
// root: output hash will store here (little-endian)
// branch: merkle branch  (big-endian)
// count: total number of merkle branch
void calc_merkle_root(unsigned char *root, int count, char **branch) {
    size_t total_count = count; // merkle branch
    unsigned char *raw_list = new unsigned char[(total_count+1)*32]; // one more space for "total_count is odd" condition
    unsigned char **list = new unsigned char*[total_count+1];

    // copy each branch to the list
    for(int i=0;i<total_count; ++i) {
        list[i] = raw_list + i * 32;
        //convert hex string to bytes array and store them into the list
        convert_string_to_little_endian_bytes(list[i], branch[i], 64);
    }
    list[total_count] = raw_list + total_count*32;

    // calculate merkle root
    while(total_count > 1) {// # remaining tree nodes > 1 
        
        if(total_count % 2 == 1) {  // if # remaining tree nodes is odd, copy the last one to make it even
            memcpy(list[total_count], list[total_count-1], 32);
        }

        int i, j;
        for(i=0, j=0; i<total_count; i+=2, j+=1) {
            // 0,1 merge to 0; 2,3 merge to 1; 4,5 merge to 2...; 64=32*2
            double_sha256((SHA256*)list[j], list[i], 64); 
        }

        total_count = j;
    }

    memcpy(root, list[0], 32);
    delete[] raw_list;
    delete[] list;
}



__global__ void find_nonce(unsigned int* found_nonce, int* found_flag){

    // volatile __shared__ BYTE smem[THREADS_PER_BLOCK*65]; // 64B for each thread

    if ( /*index*/blockIdx.x * blockDim.x + threadIdx.x <= 0xffffffff){
        HashBlock block = blockHeader_cnst;
        block.nonce = /*index*/blockIdx.x * blockDim.x + threadIdx.x;
        while ( *found_flag == 0){   
            SHA256 sha256_ctx;
            // double_sha256_dev(&sha256_ctx, (BYTE*)&block, (BYTE*)&smem[64*threadIdx.x]);
            double_sha256_dev(&sha256_ctx, (BYTE*)&block, NULL);

            // little_endian_bit_comparison
            #pragma unroll
            for (int i = 31; i >= 0; i--){
                if (sha256_ctx.b[i] > target_hex_cnst[i]) break;
                else if (sha256_ctx.b[i] < target_hex_cnst[i]) { // sha256_ctx < target_hex
                    *found_nonce = block.nonce;
                    *found_flag = 1; 
                    break;
                }
            }
            
            if (0xffffffff - block.nonce < /*stride*/gridDim.x * blockDim.x) break; // check if overflow first
            else block.nonce += /*stride*/gridDim.x * blockDim.x; 
        }
    }
}



void solve(FILE *fin, FILE *fout) {

    // **** read data *****
    char version[9];
    char prevhash[65];
    char ntime[9];
    char nbits[9];
    int tx;
    char *raw_merkle_branch;
    char **merkle_branch;

    getline(version, 9, fin); // version
    getline(prevhash, 65, fin); // previous block hash
    getline(ntime, 9, fin); // timestamp
    getline(nbits, 9, fin); // packed difficulty

    fscanf(fin, "%d\n", &tx); // # of transactions
    raw_merkle_branch = new char [tx * 65];
    merkle_branch = new char* [tx];
    for(int i=0; i<tx; ++i) {
        merkle_branch[i] = raw_merkle_branch + i * 65;
        getline(merkle_branch[i], 65, fin);
        merkle_branch[i][64] = '\0';
    }



    // **** calculate merkle root ****
    unsigned char merkle_root[32];
    calc_merkle_root(merkle_root, tx, merkle_branch);
    // printf("merkle root(little): ");
    // print_hex(merkle_root, 32);
    // printf("\n");
    // printf("merkle root(big):    ");
    // print_hex_inverse(merkle_root, 32);
    // printf("\n");



    // **** print out the header fields ****
    printf("Block info (big): \n");
    printf("  version:  %s\n", version);
    printf("  pervhash: %s\n", prevhash);
    printf("  merkleroot: "); print_hex_inverse(merkle_root, 32); printf("\n");
    printf("  ntime:    %s\n", ntime);
    printf("  nbits:    %s\n", nbits);
    printf("  nonce:    (not calc yet)\n\n");



    // **** fill in HashBlock datastructure ****
    // convert to byte array in little-endian
    HashBlock block;
    convert_string_to_little_endian_bytes((unsigned char *)&block.version, version, 8);
    convert_string_to_little_endian_bytes(block.prevhash,                  prevhash,    64);
    memcpy(block.merkle_root, merkle_root, 32);
    convert_string_to_little_endian_bytes((unsigned char *)&block.nbits,   nbits,     8);
    convert_string_to_little_endian_bytes((unsigned char *)&block.ntime,   ntime,     8);
    block.nonce = 0;
    
    

    // ********** calculate target value *********
    // calculate target value from encoded difficulty which is encoded on "nbits"
    unsigned int exp = block.nbits >> 24;
    unsigned int mant = block.nbits & 0xffffff;
    unsigned char target_hex[32] = {};
    unsigned int shift = 8 * (exp - 3);
    unsigned int sb = shift / 8;
    unsigned int rb = shift % 8;
    
    // little-endian
    target_hex[sb    ] = (mant << rb);
    target_hex[sb + 1] = (mant >> (8-rb));
    target_hex[sb + 2] = (mant >> (16-rb));
    target_hex[sb + 3] = (mant >> (24-rb));
    
    printf("Target value (big): ");
    print_hex_inverse(target_hex, 32);
    printf("\n");



    // it takes 0.00157402s to run to this point for case03.in


    // ********** find nonce **************
    // SHA256 sha256_ctx;
    // for(block.nonce=0x00000000; block.nonce<=0xffffffff;++block.nonce) {
    //     double_sha256(&sha256_ctx, (unsigned char*)&block, sizeof(block));

    //     if(little_endian_bit_comparison(sha256_ctx.b, target_hex, 32) < 0) {  // sha256_ctx < target_hex
    //         printf("Found Solution!!\n");
    //         printf("hash #%10u (big): ", block.nonce);
    //         print_hex_inverse(sha256_ctx.b, 32);
    //         printf("\n\n");
    //         break;
    //     }
    // }
    
    unsigned int* found_nonce_d;
    int* found_flag_d;
    cudaMalloc(&found_nonce_d, sizeof(unsigned int));
    cudaMalloc(&found_flag_d, sizeof(int));
    cudaMemset(found_flag_d, 0, sizeof(int));
    cudaMemcpyToSymbol(blockHeader_cnst, &block, sizeof(HashBlock));
    cudaMemcpyToSymbol(target_hex_cnst, target_hex, 32*sizeof(unsigned char));

    find_nonce<<<NUMBLOCKS, THREADS_PER_BLOCK>>>(found_nonce_d, found_flag_d);
    checkCuda(cudaDeviceSynchronize());
    
    unsigned int found_nonce_h;
    cudaMemcpy(&found_nonce_h, found_nonce_d, sizeof(unsigned int), cudaMemcpyDeviceToHost);



    // **** print result ****
    printf("A valid nonce is found.\n");
    block.nonce = found_nonce_h;
    SHA256 sha256_t;
    double_sha256(&sha256_t, (unsigned char*)&block, sizeof(block));
    printf("hash #%10u (big): ", found_nonce_h);
    print_hex_inverse(sha256_t.b, 32);
    printf("\n\n");


    for(int i=0;i<4;++i) {
        // fprintf(fout, "%02x", ((unsigned char*)&block.nonce)[i]);
        fprintf(fout, "%02x", ((unsigned char*)&found_nonce_h)[i]);
    }
    fprintf(fout, "\n");

    delete[] merkle_branch;
    delete[] raw_merkle_branch;

    cudaFree(found_nonce_d);
    cudaFree(found_flag_d);
}


int main(int argc, char **argv) {
    if (argc != 3) fprintf(stderr, "usage: hw4 <in> <out>\n");

    FILE *fin = fopen(argv[1], "r");
    FILE *fout = fopen(argv[2], "w");

    int totalblock;
    fscanf(fin, "%d\n", &totalblock);
    fprintf(fout, "%d\n", totalblock);

    for(int i=0; i<totalblock; ++i) 
        solve(fin, fout);

    return 0;
}

/***

find_nonce 
Begins: 0.607433s
Ends: 24.2085s (+23.601 s)
grid:  <<<20, 1, 1>>>
block: <<<256, 1, 1>>>
Launch Type: Regular
Static Shared Memory: 288 bytes
Dynamic Shared Memory: 0 bytes
Registers Per Thread: 164
Local Memory Per Thread: 0 bytes
Local Memory Total: 26,542,080 bytes
Shared Memory executed: 512 bytes
Shared Memory Bank Size: 4 B
Theoretical occupancy: 12.5 %
Launched from thread: 32339
Latency: ←32.105 μs
Correlation ID: 109
Stream: Default stream 7

find_nonce unroll
Begins: 0.539367s
Ends: 23.2502s (+22.711 s)
grid:  <<<20, 1, 1>>>
block: <<<256, 1, 1>>>
Launch Type: Regular
Static Shared Memory: 288 bytes
Dynamic Shared Memory: 0 bytes
Registers Per Thread: 164
Local Memory Per Thread: 0 bytes
Local Memory Total: 26,542,080 bytes
Shared Memory executed: 512 bytes
Shared Memory Bank Size: 4 B
Theoretical occupancy: 12.5 %
Launched from thread: 33228
Latency: ←14.689 μs
Correlation ID: 109
Stream: Default stream 7
*/
